{
    "default": {
        "author": "Sii Lublin",
        "firstScene": "recepcja",
        "hotSpotDebug": false,
        "showFullscreenCtrl": false,
        "autoLoad": true,
        "logoSrc": "sii.png",
        "logoHref": "http://sii.pl/"
    },
    "scenes": {
 	"recepcja": {
            "title": "Recepcja",
            "type": "cubemap",
            "cubeMap": ["locations/recepcja/front.jpg", "locations/recepcja/right.jpg", "locations/recepcja/back.jpg", "locations/recepcja/left.jpg", "locations/recepcja/up.jpg", "locations/recepcja/down.jpg"],
            "hotSpots": [
				{"pitch": -4, "yaw": 130, "type": "scene", "text": "Andromeda", "sceneId": "andromeda"},
				{"pitch": -4, "yaw": 140, "type": "scene", "text": "HR", "sceneId": "hr"},
				{"pitch": -5, "yaw": -55, "type": "scene", "text": "Kuchnia", "sceneId": "kuchnia"},
				{"pitch": -5, "yaw": -62, "type": "scene", "text": "CRM", "sceneId": "crm"}
			]
        },
 	"kuchnia": {
            "title": "Kuchnia",
            "type": "cubemap",
            "cubeMap": ["locations/kuchnia/front.jpg", "locations/kuchnia/right.jpg", "locations/kuchnia/back.jpg", "locations/kuchnia/left.jpg", "locations/kuchnia/up.jpg", "locations/kuchnia/down.jpg"],
            "hotSpots": [
				{"pitch": -5, "yaw": -11, "type": "scene", "text": "Recepcja", "sceneId": "recepcja"},
				{"pitch": -5, "yaw": 4, "type": "scene", "text": "CRM", "sceneId": "crm"},
				{"pitch": -5, "yaw": -3, "type": "scene", "text": "Administracja", "sceneId": "adm"}
			]
        },
 	"crm": {
            "title": "CRM",
            "type": "cubemap",
            "cubeMap": ["locations/crm/front.jpg", "locations/crm/right.jpg", "locations/crm/back.jpg", "locations/crm/left.jpg", "locations/crm/up.jpg", "locations/crm/down.jpg"],
            "hotSpots": [
				{"pitch": -4, "yaw": -90, "type": "scene", "text": "Kuchnia", "sceneId": "kuchnia"},
				{"pitch": -4, "yaw": -83, "type": "scene", "text": "Recepcja", "sceneId": "recepcja"},
				{"pitch": -4, "yaw": -76, "type": "scene", "text": "Administracja", "sceneId": "adm"}
			]
        },
        "adm": {
            "title": "Administracja",
            "type": "cubemap",
            "cubeMap": ["locations/adm/front.jpg", "locations/adm/right.jpg", "locations/adm/back.jpg", "locations/adm/left.jpg", "locations/adm/up.jpg", "locations/adm/down.jpg"],
            "hotSpots": [
				{"pitch": -2, "yaw": -110, "type": "scene", "text": "CRM", "sceneId": "crm"},
				{"pitch": -2, "yaw": -103, "type": "scene", "text": "AX", "sceneId": "ax"},
				{"pitch": -2, "yaw": -96, "type": "scene", "text": "Księżyc", "sceneId": "ksiezyc"}
			]
        },
 	"ksiezyc": {
            "title": "Księżyc",
            "type": "cubemap",
            "cubeMap": ["locations/ksiezyc/front.jpg", "locations/ksiezyc/right.jpg", "locations/ksiezyc/back.jpg", "locations/ksiezyc/left.jpg", "locations/ksiezyc/up.jpg", "locations/ksiezyc/down.jpg"],
            "hotSpots": [
				{"pitch": -7, "yaw": 143, "type": "scene", "text": "Administracja", "sceneId": "adm"},
				{"pitch": -7, "yaw": 150, "type": "scene", "text": "AX", "sceneId": "ax"}
			]
        },
 	"ax": {
            "title": "AX",
            "type": "cubemap",
            "cubeMap": ["locations/ax/front.jpg", "locations/ax/right.jpg", "locations/ax/back.jpg", "locations/ax/left.jpg", "locations/ax/up.jpg", "locations/ax/down.jpg"],
            "hotSpots": [
				{"pitch": -3, "yaw": -67, "type": "scene", "text": "Księżyc", "sceneId": "ksiezyc"},
				{"pitch": -3, "yaw": -60, "type": "scene", "text": "HR", "sceneId": "hr"},
				{"pitch": -3, "yaw": -53, "type": "scene", "text": "Andromeda", "sceneId": "andromeda"}
			]
        },
 	"hr": {
            "title": "HR",
            "type": "cubemap",
            "cubeMap": ["locations/hr/front.jpg", "locations/hr/right.jpg", "locations/hr/back.jpg", "locations/hr/left.jpg", "locations/hr/up.jpg", "locations/hr/down.jpg"],
            "hotSpots": [
				{"pitch": -4, "yaw": 11, "type": "scene", "text": "AX", "sceneId": "ax"},
				{"pitch": -4, "yaw": 18, "type": "scene", "text": "Andromeda", "sceneId": "andromeda"},
				{"pitch": -4, "yaw": 25, "type": "scene", "text": "Recepcja", "sceneId": "recepcja"}
			]
        },
 	"andromeda": {
            "title": "Andromeda",
            "type": "cubemap",
            "cubeMap": ["locations/andromeda/front.jpg", "locations/andromeda/right.jpg", "locations/andromeda/back.jpg", "locations/andromeda/left.jpg", "locations/andromeda/up.jpg", "locations/andromeda/down.jpg"],
            "hotSpots": [
				{"pitch": -8, "yaw": 180, "type": "scene", "text": "AX", "sceneId": "ax"},
				{"pitch": -8, "yaw": 187, "type": "scene", "text": "HR", "sceneId": "hr"},
				{"pitch": -8, "yaw": 194, "type": "scene", "text": "Recepcja", "sceneId": "recepcja"},
				{"pitch": -2, "yaw": 10, "type": "scene", "text": "Taras", "sceneId": "taras"}
			]
        },
 	"taras": {
            "title": "Taras",
            "type": "cubemap",
            "cubeMap": ["locations/taras/front.jpg", "locations/taras/right.jpg", "locations/taras/back.jpg", "locations/taras/left.jpg", "locations/taras/up.jpg", "locations/taras/down.jpg"],
            "hotSpots": [
				{"pitch": -2, "yaw": -62, "type": "scene", "text": "Andromeda", "sceneId": "andromeda"}
			]
        }
    }
}
